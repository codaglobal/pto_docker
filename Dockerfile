# Pull base image.
FROM ubuntu:latest

WORKDIR /opt

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y git npm
RUN apt-get install -y node-sqlite3
RUN apt-get install -y gcc-5 g++-5

RUN apt-get  install -y sqlite3 libsqlite3-dev

RUN git clone https://glabelle69:Jacks0n3!@bitbucket.org/codaglobal/pto-coda.git

WORKDIR /opt/pto-coda/timeoff-management
RUN npm install 
RUN npm install sqlite3
RUN npm rebuild
RUN mkdir -p /opt/pto-coda/timeoff-management/db

EXPOSE 80
CMD npm start


